﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(launch.Startup))]
namespace launch
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
